1. Membuat database
create database myshop;

2. membuat table

table users
 create table users(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

table categories
create table categories(
    -> id int(8) primary key auto_increment,
    -> name varchar(255)
    -> );

table items
create table items(
    -> id int(8) primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(20),
    -> stock int(20),
    -> category_id int(8),
    -> foreign key(category_id) references categories(id)
    -> );


3.menambahkan data pada table

menambahkan data table users
insert into users(name, email, password) values("John Doe", "john@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");

menambahkan data table categories
insert into categories(name) values("gadget"), ("cloth", "men"), ("women",) ("branded");

menambahkan data table items
insert into items(name, description, price, stock, category_id) values("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);


4. Mengambil data dari database

a. mengambil data users (kecuali password)
select id, name, email from users;

b. - mengambil data item pada table items yg memiliki harga diatas 1jt
select * from items where price>1000000;
b. - mengambil data item pada table items yg memiliki name serupa atau mirip dengan kata kunci "uniklo"
select * from items where name like '%uniklo%';

c. menampilkan data items join dengan kategori
select items.name, items.description, items.price, items.stock, items.category_id, categories.name as kategori from items inner join categories on items.category_id = categories.id;


5. Mengubah data dari database
update items set price=2500000 where id=1;
